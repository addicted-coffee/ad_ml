import pandas as pd
import quandl
import math

# dataframe of a stock
df = quandl.get('WIKI/GOOGL')

# params used to define the label
# column that we gonna forecast
forecast_col = 'Adj. Close'
# percentage of the dataframe used to forecast
out_factor = 0.01

# filtered only meanifull features from the dataframe
df = df[['Adj. Open', 'Adj. High', 'Adj. Low', 'Adj. Close', 'Adj. Volume']]

# High minus Low Percentage or the volatilty percent of the stock on da daily basis
df['HL_PCT'] = (df['Adj. High'] - df['Adj. Close']) / df['Adj. Close'] * 100.0

# Daily percent change (new - old)/old
df['PCT_change'] = (df['Adj. Close'] - df['Adj. Open']) / df['Adj. Open'] * 100.0

# new dataframe with only the usefull features
df = df[['Adj. Close', 'HL_PCT', 'PCT_change', 'Adj. Volume']]

# replace invalid data
df.fillna(-99999, inplace=True)

# forecast using 10% of the dataframe
forecast_out = int(math.ceil(out_factor*len(df)))

# now the label column is the forecast column defined
df['label'] = df[forecast_col].shift(-forecast_out)
df.dropna(inplace=True)

print(df.head())
